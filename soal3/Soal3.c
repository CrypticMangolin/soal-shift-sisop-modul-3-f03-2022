#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <ctype.h>
#include <errno.h>
#include <pthread.h>

char *path_to_folder = "/home/nao/shift3/hartakarun/";

int total_files = 0;
int status = 0;
char infos[1000][1000];
pthread_t tid[1000];

void list_file_recursively(char *base_path)
{
  char path[1024];
  struct dirent *dp;
  DIR *dir = opendir(base_path);

  if (!dir) return;

  while ((dp = readdir(dir)) != NULL) {
    if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0) {
      snprintf(path, sizeof path, "%s/%s", base_path, dp->d_name);
      char *check = strrchr(path, '/');
      if (check[1] == '.') {
        mkdir("hidden", 0777);
        char moves[10000];
        snprintf(moves, sizeof moves, "hidden/%s", dp->d_name);
        rename(path, moves);
      } else {
        char loc[1000];
        strcpy(loc, path);
        snprintf(infos[total_files], sizeof loc, "%s", loc);
        total_files += 1;
      }
      list_file_recursively(path);
    }
  }
  closedir(dir);
}

char* get_folder_name(char *path_to_folder)
{
  char *folder_name ;
  char *ret;
  ret = strrchr(path_to_folder, '/'); 
  if (ret != NULL) {
    if (ret[1] == '.') {
      return "hidden";
    }
    folder_name = strtok(path_to_folder, ".");
    folder_name = strtok(NULL, "");
    if (folder_name == NULL) {
        return "unknown";
    }

    for (int i = 0; folder_name[i]; ++i){
        folder_name[i] = tolower(folder_name[i]);
    }
    return folder_name;
  }
}

char* get_file_name(char *path_to_file)
{
  char *temp = strtok(path_to_file, "/");
  int count = 0;
  char *file_name[10];
  while (temp != NULL) {
    file_name[count] = temp;
    temp = strtok(NULL, "/");
    count++;
  }
  return file_name[count - 1];
}

void* move_file(char *file_buffer, char *file_name, char *folder_name)
{
  char newname[2000];
  
  printf("%s %s\n %s %s", file_name, folder_name, file_buffer, newname);
}

void* process_file(void *path_to_file)
{
  char *folder_name, *file_name;
  char *file_path, newPath[10000];

  file_path = strdup((char *) path_to_file);
  DIR *dir_path = opendir(file_path);

  if (dir_path == NULL)
  {
    folder_name = get_folder_name(path_to_file);
    file_name = get_file_name(path_to_file);
    mkdir(folder_name, 0777);
    if (strcmp(folder_name, "unknown") == 0 || strcmp(folder_name, "hidden") == 0 ) {
      snprintf(newPath, sizeof newPath, "%s/%s", folder_name, file_name);
    } else {
      snprintf(newPath, sizeof newPath, "%s/%s.%s", folder_name, file_name, folder_name);
    }
    rename(file_path, newPath);
  }
}

int main()
{
  list_file_recursively(path_to_folder);
  for (int i = 0; i < total_files; ++i) {
    pthread_create(&(tid[i]), NULL, process_file, (char*) infos[i]);
  }
  
  for (int i = 0; i <= total_files; ++i){
    pthread_join(tid[i], NULL);
  }
  
  exit(EXIT_SUCCESS);
}
