# Soal Shift Sisop Modul 3 F03-2022 

## Anggota Kelompok

| Nama                       | NRP        |
| :------------------------- | :--------- |
| Hidayatullah               | 5025201031 |
| Muhammad Daffa Aldriantama | 5025201177 |
| Atha Dzaky Hidayanto       | 5025201269 |


## Soal 1
### Penjelasan
Soal nomor 1 memerintahkan kita untuk melakukan decode dari beberapa file txt, yaitu 9 file `q` yang berada di dalam folder `quote` dan 9 file `m` yang berada di dalam folder `music`. Setelah melakukan decode, file txt yang berisi hasil dari decode tersebut dipindahkan ke dalam folder `hasil` yang akan di-compress ke dalam `hasil.zip`.

### a. Unzip quote.zip dan music.zip secara bersamaan
Untuk melakukan unzip, pertama kita perlu membuat funtion yang memanggil command `unzip`. Potongan kode di bawah ini adalah fungsi yang melakukan hal tersebut.

```c
void unzip(char* fileName) {
    int status;
    child = fork();
    char fileDir[100] = "/home/duffaldri/Desktop/Sisop/modul3/";
    char destDir[100];
    strcat(fileDir, fileName);
    strcpy(destDir, fileDir);
    strcat(fileDir, ".zip");

    if(child == 0) {
        char *argv[] = {"unzip", "-o", fileDir, "-d", destDir, NULL}; 
            execv("/usr/bin/unzip", argv);
    }
}
```

Kemudian, fungsi tersebut dipanggil di dalam fungsi `run()` yang digunakan untuk menentukan apa yang akan dilakukan oleh suatu thread.

```c
void* run() {
    pthread_t id = pthread_self();

    if(pthread_equal(id, tid[0])) {
        unzip("quote");
    } else if(pthread_equal(id, tid[1])) {
        unzip("music");
    }
    return NULL;
}   
```

Terakhir, kita melakukan pembuatan thread di dalam main function dengan cara sebagai berikut.

```c
...
  pid_t child1_id = fork();

    if(child1_id < 0) {
        exit(EXIT_FAILURE);
    } else if (child1_id == 0) {
        for(int i = 0; i < 2; i++) {    
            err = pthread_create(&(tid[i]), NULL, &run, NULL);
            if(err) {
                printf("\n can't create thread : [%s]",strerror(err));

            } else {
                printf("\n create thread success %d\n", i);
            }
        }
        pthread_join(tid[0],NULL);
        pthread_join(tid[1],NULL);
...
```

Berikut adalah hasil yang terlihat di dalam folder.

[![https://imgur.com/KZcM6NO.png](https://imgur.com/KZcM6NO.png)](https://imgur.com/KZcM6NO.png)

[![https://imgur.com/O2w1xQP.png](https://imgur.com/O2w1xQP.png)](https://imgur.com/O2w1xQP.png)

[![https://imgur.com/u05KWEq.png](https://imgur.com/u05KWEq.png)](https://imgur.com/u05KWEq.png)

### b. Decode File TXT dengan Base64
Untuk melakukan decode, kami menggunakan command `base64` yang dapat dipanggil menggunakan CLI. Untuk memanggil command tersebut dengan program C, kami menggunakan `execl`. Berikut potongan kode dari fungsi tersebut.

```c
void b64(char* cmd) {
    pid_t child_id = fork();
    int status;

    if(child_id < 0) {
        exit(EXIT_FAILURE);
    } else if(child_id == 0) {
        // puts("BERHASIL");
        execl("/bin/sh", "sh", "-c", cmd, 0);
    } else {
        while((wait(&status)) > 0);
    }
}
```

Setelah dibuat, fungsi tersebut dipanggil dengan fungsi `decode()` yang menentukan apa yang akan dilakukan suatu thread. 

Fungsi `decode()`

```c
void* decode() {
    pthread_t id = pthread_self();

    if(pthread_equal(id, tid[2])) {
        for(int i = 1; i <= 9; i++) {
            char cmd[120];
            sprintf(cmd, "base64 -d /home/duffaldri/Desktop/Sisop/modul3/quote/q%d.txt >> /home/duffaldri/Desktop/Sisop/modul3/quote.txt", i);
            b64(cmd);
            newLine("/home/duffaldri/Desktop/Sisop/modul3/quote.txt");
            sleep(0.001);
        }
    } else if(pthread_equal(id, tid[3])) {
        for(int i = 1; i <= 9; i++) {
            char cmd[120];
            sprintf(cmd, "base64 -d /home/duffaldri/Desktop/Sisop/modul3/music/m%d.txt >> /home/duffaldri/Desktop/Sisop/modul3/music.txt", i);
            b64(cmd);
            newLine("/home/duffaldri/Desktop/Sisop/modul3/music.txt");
            sleep(0.001);
        }
    
    }

    return NULL;
}
```

Terakhir, kita melakukan pembuatan thread di dalam main function dengan cara sebagai berikut.

```c
...
while(wait(&status) > 0);

pid_t child2_id = fork();

if(child2_id < 0) {
    exit(EXIT_FAILURE);
} else if (child2_id == 0) {
    for(int i = 2; i < 4; i++) {    
        err = pthread_create(&(tid[i]), NULL, &decode, NULL);
        if(err) {
            printf("\n can't create thread : [%s]",strerror(err));

        } else {
            printf("\n create thread success %d\n", i);
        }
    }
    pthread_join(tid[2],NULL);
    pthread_join(tid[3],NULL);
...
```

![https://imgur.com/fAjMgWu.png](https://imgur.com/fAjMgWu.png)


![https://imgur.com/37CmfWP.png](https://imgur.com/37CmfWP.png)


### c. Memindahkan file .txt ke folder hasil
Karena tidak diminta untuk dipindahkan secara bersamaan, kita tidak perlu menggunakan thread. Pemindahan file ke dalam folder `hasil` dapat dilakukan dengan menggunakan command `mv`. Command `mv` dipanggil dengan menggunakan `execv`. Perintah-perintah tersebut dimasukkan ke dalam fungsi `moveFiles()` sebagai berikut.

```c
void moveFiles() {
    pid_t child_id;
    child_id = fork();
    int status;

    if(child_id < 0) {
        exit(EXIT_FAILURE);
    } else if(child_id == 0) { 
            char *argv[] = {"mkdir", "-p","/home/duffaldri/Desktop/Sisop/modul3/hasil", NULL};
            execv("/bin/mkdir", argv);
    } else {
        while(wait(&status) > 0);
        pid_t child2_id;
        child2_id = fork();

        if(child2_id == 0) {
            char *argv[] = {"mv", "/home/duffaldri/Desktop/Sisop/modul3/music.txt", "/home/duffaldri/Desktop/Sisop/modul3/hasil", NULL}; 
            execv("/usr/bin/mv", argv);
        } else {
            while(wait(&status) > 0);
            pid_t child3_id;
            child3_id = fork();

            if(child3_id == 0) {
                char *argv[] = {"mv", "/home/duffaldri/Desktop/Sisop/modul3/quote.txt", "/home/duffaldri/Desktop/Sisop/modul3/hasil", NULL}; 
                execv("/usr/bin/mv", argv);
            }
    
            while(wait(&status) > 0);
        }
    }
}
```

[![https://imgur.com/GiBCfey.png](https://imgur.com/GiBCfey.png)](https://imgur.com/GiBCfey.png)

### d. Melakukan Compress Folder
Soal memerintahkan kami untuk melakukan zip dan memberikan password untuk file zip tersebut. Untuk melakukannya, kita dapat menggunakan command bash `zip` yang dipanggil menggunakan `execv()` di dalam program C. Berikut merupakan fungsi `zip()`.

```c
void zip(char* name, char* pass) {
    pid_t child_id;
    child_id = fork();
    int status;

    if(child_id < 0) {
        exit(EXIT_FAILURE);
    } else if(child_id == 0) { 
        char *argv[] = {"zip", "-P", pass, "-rm", "hasil.zip", name, NULL};
        execv("/usr/bin/zip", argv);
    }
    
    while(wait(&status) > 0);
}
```

![https://imgur.com/SFLpJD8.png](https://imgur.com/SFLpJD8.png)

![https://imgur.com/0z4BEsJ.png](https://imgur.com/0z4BEsJ.png)

![https://imgur.com/scRl8pr.png](https://imgur.com/scRl8pr.png)

### e. Unzip, Menambahkan File No.txt, dan Zip Kembali (Revisi)
Dalam poin e, dapat disimpulkan bahwa akan terdapat dua tahap, yaitu "unzip dan membuat file No.txt" dan "zip folder hasil dan No.txt".

#### Tahap Pertama
Untuk menyelesaikan tahap pertama, kami menggunakan thread agar keduanya dapat dilakukan secara bersamaan.

Pertama, buat function yang dibutuhkan, yaitu `unzipPass()`, digunakan untuk melakukan unzip file yang memiliki password, dan `createFile()`, digunakan untuk membuat file `No.txt`.

`unzipPass()` tidak berbeda jauh dengan `unzip()`. Keduanya hanya dibedakan oleh penggunaan password.
```c

void unzipPass(char* fileName, char* pass) {
    pid_t child_id;
    child_id = fork();
    int status;

    char fileDir[100] = "/home/duffaldri/Desktop/Sisop/modul3/";
    char destDir[100];
    strcpy(destDir, fileDir);
    strcat(fileDir, fileName);
    strcat(fileDir, ".zip");

    if(child_id < 0) {
        exit(EXIT_FAILURE);
    } else if(child_id == 0) { 
            char *argv[] = {"unzip", "-P", pass, "-o", fileDir, "-d", destDir, NULL};
            execv("/usr/bin/unzip", argv);
    }
    
    while(wait(&status) > 0);
}
```
`createFile()` menggunakan *file pointer* untuk membuat, membuka, dan menulis di dalam file tersebut.
```c

void createFile(char* fileName, char* text) {
    FILE* fp;
    char fileDir[100] = "/home/duffaldri/Desktop/Sisop/modul3/";
    strcat(fileDir, fileName);

    fp = fopen(fileDir, "w+");

    if(fp == NULL) exit(EXIT_FAILURE);

    fprintf(fp, "%s", text);

    fclose(fp);
}
```

Setelah itu, kita melakukan hal yang sama dengan poin (a), yaitu membuat fungsi yang digunakan untuk menentukan apa yang dilakukan oleh suatu thread.

```c
void* runNo3() {
    pthread_t id = pthread_self();

    if(pthread_equal(id, tid[0])) {
        unzipPass("hasil", password);
    } else if(pthread_equal(id, tid[1])) {
        createFile("No.txt", "No");
    }
    return NULL;
}
```

Terakhir, buat thread dengan menggunakan fungsi `pthread_create()`.

```c
void no3() {
    int err;
    int status;

    pid_t child1_id = fork();

    if(child1_id < 0) {
        exit(EXIT_FAILURE);
    } else if (child1_id == 0) {
        for(int i = 0; i < 2; i++) {    
            err = pthread_create(&(tid[i]), NULL, &runNo3, NULL);
            if(err) {
                printf("\n can't create thread : [%s]",strerror(err));

            } else {
                printf("\n create thread success no 3 %d\n", i);
            }
        }
        pthread_join(tid[0],NULL);
        pthread_join(tid[1],NULL);
    }
...
```

[![https://imgur.com/mkMQ6Qr.png](https://imgur.com/mkMQ6Qr.png)](https://imgur.com/mkMQ6Qr.png)

#### Tahap Kedua
Untuk tahap kedua, yang perlu kita lakukan hanyalah memanggil function `zip()` yang telah dibuat pada poin sebelumnya.

```c
...
zip("hasil", password);
zip("No.txt", password);
...
```

Setelah itu semua, kita hanya tinggal menggabungkan kedua tahap tersebut ke dalam 1 fungsi sehingga akan didapat fungsi berikut ini.

```c
void no3() {
    int err;
    int status;

    pid_t child1_id = fork();

    if(child1_id < 0) {
        exit(EXIT_FAILURE);
    } else if (child1_id == 0) {
        for(int i = 0; i < 2; i++) {    
            err = pthread_create(&(tid[i]), NULL, &runNo3, NULL);
            if(err) {
                printf("\n can't create thread : [%s]",strerror(err));

            } else {
                printf("\n create thread success no 3 %d\n", i);
            }
        }
        pthread_join(tid[0],NULL);
        pthread_join(tid[1],NULL);
    } else {
        while(wait(&status) > 0); 
        zip("hasil", password);
        zip("No.txt", password);
    }
    while(wait(&status) > 0); 
}
```

[![https://imgur.com/iiKfhmT.png](https://imgur.com/iiKfhmT.png)](https://imgur.com/iiKfhmT.png)

[![https://imgur.com/fjCcWG8.png](https://imgur.com/fjCcWG8.png)](https://imgur.com/fjCcWG8.png)

[![https://imgur.com/7GIXuTp.png](https://imgur.com/7GIXuTp.png)](https://imgur.com/7GIXuTp.png)

---

## Nomor 3

### Cara Pengerjaan
Sebelum memulai archive zip hartakarun di extract ke dalam `home/[user]/shift3/hartakarun`.

Semua file dan folder di list ke dalam suatu array global dengan menggunakan fungsi berikut.
```c++
void* list_file_recursively(char *base_path)
{
  char path[1000];
  struct dirent *dp;
  DIR *dir = opendir(base_path);

  if (!dir) return;

  while ((dp = readdir(dir)) != NULL) {
    if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0) {
      snprintf(path, sizeof path, "%s/%s", base_path, dp->d_name);
      char *check = strrchr(path, '/');
      if (check[1] == '.') {
        mkdir("hidden", 0777);
        char moves[10000];
        snprintf(moves, sizeof moves, "hidden/%s", dp->d_name);
        rename(path, moves);
      } else {
        char loc[1000];
        strcpy(loc, path);
        snprintf(infos[total_files], sizeof loc, "%s", loc);
        total_files += 1;
      }
      list_file_recursively(path);
    }
  }
  closedir(dir);
}
```

Proses pengkategorian menggunakan thread tiap file, yang dibuat dengan loop dan fungsi berikut.

```c++
int main()
{
  ...
  for (int i = 0; i < total_files; ++i) {
    pthread_create(&(tid[i]), NULL, process_file, (char*) infos[i]);
  }
  ...
}

void* process_file(void *path_to_file)
{
  char *folder_name, *file_name;
  char *file_path, file_buffer[10000];

  file_path = strdup((char *) path_to_file);
  DIR *dir_path = opendir(file_path);

  if (dir_path == NULL)
  {
    folder_name = get_folder_name(path_to_file);
    file_name = get_file_name(path_to_file);
    ...
  }
}
```

Nama folder diambil dengan membuat string baru yg dimulai dari `/` terakhir. Dilakukan pengecekan jika karakter setelah `/` ialah `.` maka termasuk `Hidden`. 
```c++
char* get_folder_name(char *path_to_folder)
{
  char *folder_name ;
  char *ret;
  ret = strrchr(path_to_folder, '/'); 
  if (ret != NULL) {
    if (ret[1] == '.') {
      return "hidden";
    }
    ...
  }
}
```
Jika tidak ditemukan substring sebelum `.` make dikategorikan unknown. Jika bukan keduanya, ambil substring setelah titik pertama dalam huruf kecil. 
```c++
char* get_folder_name(char *path_to_folder)
{
  {...
    folder_name = strtok(path_to_folder, "."); // ambil substring sebelum "."
    folder_name = strtok(NULL, ""); // ambil substring setelah "." (akan return null jika tidak ada titik)
    if (folder_name == NULL) {
        return "unknown";
    }

    for (int i = 0; folder_name[i]; ++i){
        folder_name[i] = tolower(folder_name[i]);
    }
    return folder_name;
  }
}
```

Untuk mendapatkan nama filenya menggunakan token yg dihasilkan menggunakan strtok terakhir.

```c++
char* get_file_name(char *path_to_file)
{
  char *temp = strtok(path_to_file, "/");
  int count = 0;
  char *file_name[10];
  while (temp != NULL) {
    file_name[count] = temp;
    temp = strtok(NULL, "/");
    count++;
  }
  return file_name[count - 1];
}
```
Membuat folder menggunakan fungsi mkdir, dan rename untuk memindahkan file.
```c++
void* process_file(void *path_to_file)
{   
    ...
    mkdir(folder_name, 0777);
    if (strcmp(folder_name, "unknown") == 0 || strcmp(folder_name, "hidden") == 0 ) {
      snprintf(newPath, sizeof newPath, "%s/%s", folder_name, file_name);
    } else {
      snprintf(newPath, sizeof newPath, "%s/%s.%s", folder_name, file_name, folder_name);
    }
    rename(file_path, newPath);
}
```

### client.c

Zip folder hartakarun:

```c++
void* zip (void *DIR)
{
	int status;
	if (fork() == 0) {
		char *argv [] = {"zip", "-r", FILENAME, DIR, NULL};
		execv("/bin/zip", argv);
	} else {
	    while((wait(&status))>0);
    }
}
```

Mendapatkan argumen "send hartakarun.zip":

```c++
...
  if (argc > 1) {
    if (strcmp(argv[1], "send") == 0) {
      if (argc > 2) {
        file_name = argv[2];
      } else {
        exit(EXIT_FAILURE);
      }
    } else {
      exit(EXIT_FAILURE);
    }
  }
...
```

Menyiapkan koneksi socket dan connect dengan server

```c++
    ...
    struct sockaddr_in address;
    int sock = 0;
    struct sockaddr_in serv_addr;

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Socket creation error \n");
        return -1;
    }
  
    memset(&serv_addr, '0', sizeof(serv_addr));
  
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
    
    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }
  
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("\nConnection Failed \n");
        return -1;
    }
    ...
```

Menyipakan file untuk membaca file zip, dan mulai mengirim data menggunakan `write`.

```c++
	ssize_t len;
    int fd;
    char buffer[BUFSIZ];

    fd = open(FILENAME, O_RDONLY);
    if (fd == -1) {
        printf("\nOpening File Failed \n");
        exit(EXIT_FAILURE);
    }

    while(1) {
        len = read(fd, buffer, BUFSIZ);
        if (len == 0) break;
        if (len == -1) {
            printf("\nReading File Failed \n");
            exit(EXIT_FAILURE);
        }
        if (write(sock, buffer, len) == -1) {
            printf("\nReading File Failed \n");
            exit(EXIT_FAILURE);
        }
    }

    return 0;
}
```

### server.c

Menyiapkan koneksi dan menerima socket client.

```c++
int main(int argc, char const *argv[])
{
    int server_socket, new_socket;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);

    if ((server_socket = socket(AF_INET, SOCK_STREAM, 0)) == 0)
    {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    if (setsockopt(server_socket, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt)))
    {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    if (bind(server_socket, (struct sockaddr *)&address, sizeof(address)) < 0)
    {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_socket, 5) < 0)
    {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    if ((new_socket = accept(server_socket, (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0)
    {
        perror("accept");
        exit(EXIT_FAILURE);
    }
    ...
}
```
Menyiapkan file buffer untuk menerima data dari client. Setelah itu ditulis melalui file descriptor `received_file`.
```c++
    ...
    char buffer[BUFSIZ];`
    int received_file;
    ssize_t len;

    received_file = open(FILENAME, O_WRONLY | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR);
    if (received_file == -1)
    {
        perror("open");
        exit(EXIT_FAILURE);
    }

    do
    {
        len = read(new_socket, buffer, BUFSIZ);
        if (len == -1)
        {
            perror("read");
            exit(EXIT_FAILURE);
        }
        if (write(received_file, buffer, len) == -1)
        {
            perror("write");
            exit(EXIT_FAILURE);
        }
    } while (len > 0);
    close(received_file);
    close(new_socket);
    close(server_socket);

    return 0;
}
```

### proses run

File hartakarun.zip harus di-extract terlebih dahulu ke `/home/[user]/shift3/`
![Extract](https://imgur.com/gD3db63.png)

Run Soal3.c untuk mengkategorikan file.
```bash
gcc -pthread -o /home/[user]/shift3/hartakarun/soal3 Soal3.c
cd /home/[user]/shift3/hartakarun
./soal3
```
![Category](https://imgur.com/BYrxOnY.png)

Compile dan run Server.c dan Client.c Pada direktorinya masing-masing.
```bash
gcc client.c -o client # pada direktori Client/
gcc server.c -o server # pada direktori Server/

./server # server dijalankan terlebih dahulu
./client send hartakarun.zip # client mulai dijalankan
```
![Server](https://imgur.com/F4Q0dUl.png)
![Client](https://imgur.com/fLuPpPo.png)

File Zip terkirim
![ZipServ](https://imgur.com/fLuPpPo.png)
