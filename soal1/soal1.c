#include<stdio.h>
#include<string.h>
#include<pthread.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/wait.h>

pthread_t tid[4];
pid_t child;

char* user = "daffa";
char password[25] = "mihinomenest";

void newLine(char* dir) {
    pid_t child_id = fork();
    int status;

    if(child_id < 0) {
        exit(EXIT_FAILURE);
    } else if(child_id == 0) {
        char cmd[30] = "echo "" >> ";
        strcat(cmd, dir);
        execl("/bin/sh", "sh", "-c", cmd, 0);
    } else {
        while((wait(&status)) > 0);
    }
}

void b64(char* cmd) {
    pid_t child_id = fork();
    // char dir[100] = "/home/duffaldri/Desktop/Sisop/modul3/no3/";
    // strcat(dir, name);
    // strcat(dir, "/");

    // char file[100];
    // strcpy(file, dir);
    // strcat(file, fileName);
    // strcat(file, ".txt");
    // strcat(cmd, " > ");
    int status;

    if(child_id < 0) {
        exit(EXIT_FAILURE);
    } else if(child_id == 0) {
        // puts("BERHASIL");
        execl("/bin/sh", "sh", "-c", cmd, 0);
    } else {
        while((wait(&status)) > 0);
    }
}

void* decode() {
    pthread_t id = pthread_self();

    if(pthread_equal(id, tid[2])) {
        for(int i = 1; i <= 9; i++) {
            char cmd[120];
            sprintf(cmd, "base64 -d /home/duffaldri/Desktop/Sisop/modul3/quote/q%d.txt >> /home/duffaldri/Desktop/Sisop/modul3/quote.txt", i);
            b64(cmd);
            newLine("/home/duffaldri/Desktop/Sisop/modul3/quote.txt");
            sleep(0.001);
        }
    } else if(pthread_equal(id, tid[3])) {
        for(int i = 1; i <= 9; i++) {
            char cmd[120];
            sprintf(cmd, "base64 -d /home/duffaldri/Desktop/Sisop/modul3/music/m%d.txt >> /home/duffaldri/Desktop/Sisop/modul3/music.txt", i);
            b64(cmd);
            newLine("/home/duffaldri/Desktop/Sisop/modul3/music.txt");
            sleep(0.001);
        }
    
    }

    return NULL;
}

void unzip(char* fileName) {
    int status;
    child = fork();
    char fileDir[100] = "/home/duffaldri/Desktop/Sisop/modul3/";
    char destDir[100];
    strcat(fileDir, fileName);
    strcpy(destDir, fileDir);
    strcat(fileDir, ".zip");

    if(child == 0) {
        char *argv[] = {"unzip", "-o", fileDir, "-d", destDir, NULL}; 
            execv("/usr/bin/unzip", argv);
    }
}

void unzipPass(char* fileName, char* pass) {
    pid_t child_id;
    child_id = fork();
    int status;

    char fileDir[100] = "/home/duffaldri/Desktop/Sisop/modul3/";
    char destDir[100];
    strcpy(destDir, fileDir);
    strcat(fileDir, fileName);
    strcat(fileDir, ".zip");

    if(child_id < 0) {
        exit(EXIT_FAILURE);
    } else if(child_id == 0) { 
            char *argv[] = {"unzip", "-P", pass, "-o", fileDir, "-d", destDir, NULL};
            execv("/usr/bin/unzip", argv);
    }
    
    while(wait(&status) > 0);
}

void* run() {
    pthread_t id = pthread_self();

    if(pthread_equal(id, tid[0])) {
        unzip("quote");
    } else if(pthread_equal(id, tid[1])) {
        unzip("music");
    }
    return NULL;
}

void moveFiles() {
    pid_t child_id;
    child_id = fork();
    int status;

    if(child_id < 0) {
        exit(EXIT_FAILURE);
    } else if(child_id == 0) { 
            char *argv[] = {"mkdir", "-p","/home/duffaldri/Desktop/Sisop/modul3/hasil", NULL};
            execv("/bin/mkdir", argv);
    } else {
        while(wait(&status) > 0);
        pid_t child2_id;
        child2_id = fork();

        if(child2_id == 0) {
            char *argv[] = {"mv", "/home/duffaldri/Desktop/Sisop/modul3/music.txt", "/home/duffaldri/Desktop/Sisop/modul3/hasil", NULL}; 
            execv("/usr/bin/mv", argv);
        } else {
            while(wait(&status) > 0);
            pid_t child3_id;
            child3_id = fork();

            if(child3_id == 0) {
                char *argv[] = {"mv", "/home/duffaldri/Desktop/Sisop/modul3/quote.txt", "/home/duffaldri/Desktop/Sisop/modul3/hasil", NULL}; 
                execv("/usr/bin/mv", argv);
            }
    
            while(wait(&status) > 0);
        }
    }
}

void createFile(char* fileName, char* text) {
    FILE* fp;
    char fileDir[100] = "/home/duffaldri/Desktop/Sisop/modul3/";
    strcat(fileDir, fileName);

    fp = fopen(fileDir, "w+");

    if(fp == NULL) exit(EXIT_FAILURE);

    fprintf(fp, "%s", text);
}

void zip(char* name, char* pass) {
    pid_t child_id;
    child_id = fork();
    int status;

    if(child_id < 0) {
        exit(EXIT_FAILURE);
    } else if(child_id == 0) { 
        char *argv[] = {"zip", "-P", pass, "-rm", "hasil.zip", name, NULL};
        execv("/usr/bin/zip", argv);
    }
    
    while(wait(&status) > 0);
}

void* runNo3() {
    pthread_t id = pthread_self();

    if(pthread_equal(id, tid[0])) {
        unzipPass("hasil", password);
    } else if(pthread_equal(id, tid[1])) {
        createFile("No.txt", "No");
    }
    return NULL;
}

void no3() {
    int err;
    int status;

    pid_t child1_id = fork();

    if(child1_id < 0) {
        exit(EXIT_FAILURE);
    } else if (child1_id == 0) {
        for(int i = 0; i < 2; i++) {    
            err = pthread_create(&(tid[i]), NULL, &runNo3, NULL);
            if(err) {
                printf("\n can't create thread : [%s]",strerror(err));

            } else {
                printf("\n create thread success no 3 %d\n", i);
            }
        }
        pthread_join(tid[0],NULL);
        pthread_join(tid[1],NULL);
    } else {
        while(wait(&status) > 0); 
        zip("hasil", password);
        zip("No.txt", password);
    }
    while(wait(&status) > 0); 
}

int main() {
    strcat(password, user);
    int status;

    int err;

    pid_t child1_id = fork();

    if(child1_id < 0) {
        exit(EXIT_FAILURE);
    } else if (child1_id == 0) {
        for(int i = 0; i < 2; i++) {    
            err = pthread_create(&(tid[i]), NULL, &run, NULL);
            if(err) {
                printf("\n can't create thread : [%s]",strerror(err));

            } else {
                printf("\n create thread success %d\n", i);
            }
        }
        pthread_join(tid[0],NULL);
        pthread_join(tid[1],NULL);
    } else {
        while(wait(&status) > 0);
    
        pid_t child2_id = fork();

        if(child2_id < 0) {
            exit(EXIT_FAILURE);
        } else if (child2_id == 0) {
            for(int i = 2; i < 4; i++) {    
                err = pthread_create(&(tid[i]), NULL, &decode, NULL);
                if(err) {
                    printf("\n can't create thread : [%s]",strerror(err));

                } else {
                    printf("\n create thread success %d\n", i);
                }
            }
            pthread_join(tid[2],NULL);
            pthread_join(tid[3],NULL);
        } else {
            while(wait(&status) > 0);
            moveFiles();
            zip("hasil", password);
            sleep(2);
            no3();
            while(wait(&status) > 0);
            printf("Selesai\n");
        }
        

    }

    return 0;
}
