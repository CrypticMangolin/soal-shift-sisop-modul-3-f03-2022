#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/sendfile.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <pthread.h>
#include <wait.h>

#define PORT 8080
#define FILENAME        "./hartakarun.zip"

void* zip (void *DIR) {
	int status;
	if (fork() == 0) {
		char *argv [] = {"zip", "-r", FILENAME, DIR, NULL};
		execv("/bin/zip", argv);
	} else {
	    while((wait(&status))>0);
    }
}

int main(int argc, char const *argv[]) {
	char *DIR = "/home/nao/shift3/hartakarun";
	char* file_name = "hartakarun.zip";

    zip(DIR);

    if (argc > 1) {
        if (strcmp(argv[1], "send") == 0) {
                if (argc > 2) {
                    strcpy(file_name, argv[2]);
                } else {
                    exit(EXIT_FAILURE);
                }
            } else {
            exit(EXIT_FAILURE);
        }
    }


    struct sockaddr_in address;
    int sock = 0;
    struct sockaddr_in serv_addr;

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Socket creation error \n");
        return -1;
    }
  
    memset(&serv_addr, '0', sizeof(serv_addr));
  
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
    
    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }
  
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("\nConnection Failed \n");
        return -1;
    }	
	
	ssize_t len;
    int fd;
    char buffer[BUFSIZ];

    fd = open(FILENAME, O_RDONLY);
    if (fd == -1) {
        printf("\nOpening File Failed \n");
        exit(EXIT_FAILURE);
    }

    while(1) {
        len = read(fd, buffer, BUFSIZ);
        if (len == 0) break;
        if (len == -1) {
            printf("\nReading File Failed \n");
            exit(EXIT_FAILURE);
        }
        if (write(sock, buffer, len) == -1) {
            printf("\nReading File Failed \n");
            exit(EXIT_FAILURE);
        }
    }

    return 0;
}